<?php
class MY_Controller extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    protected $inUser;
    private $inPermition;
    protected $inPage=1;
    protected $inLang=0;
    protected $inLangName='';
    protected $inFooterMenu=false;
    protected $inModule;
    protected $PropertySearch = "";
    protected $inPicPath = "images/";
    protected $inImgPath = "images/";
    protected $inVideoPath = "images/";
    //
    function __construct() {
        parent::__construct();
    }
    //
    public function getImages($aImageId,$aImageWidth, $aImageHeight, $aSpectRatio=true){
    }
    // ремапинг вызова
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    // вывод в браузер
    public function _output($output) {
        echo $output;  
    }
    // чтение языка при необходимости
    protected function getLang() {
        return $this->inLang;
    }
    // расширение базового класса, вызова стартовой функцией
    protected function startUp() {
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->load->library('Captcha');
    }
    //
    protected function includeUp($aMenuRecursive=false) {
        $inData = array();
        return $inData;
    }
    // чтение пост вызова стартовой функции
    protected function afterInclude($aData=array()) {
        return $aData;
    }
    // описание функции если необходим поиск на сайте, переопределяется в дочерних классах
    protected function goSearch() {
    }
} 
